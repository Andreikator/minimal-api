var builder = WebApplication.CreateBuilder(args);
var hotels = new List<Hotel>();
//ConfigureServices

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var app = builder.Build();
//Configure
app.UseSwagger();
app.UseSwaggerUI();
app.MapGet("/hotels", () =>hotels );
app.MapGet("/hotels/{id}", (int id) => hotels.FirstOrDefault(h => h.Id == id));
app.MapPost("/hotels", (Hotel hotel) =>
{
    var index = hotels.FindIndex(h => h.Id == hotel.Id);
    if (index == 0)
    {
        throw new Exception("Id already in use");
    }
    hotels.Add(hotel);
});
app.MapPut("/hotels",(Hotel hotel) => {
    var index = hotels.FindIndex(h => h.Id == hotel.Id);
    if (index < 0)
    {
        throw new Exception("Not found");
    }
    hotels[index] = hotel;
});
app.MapDelete("/hotels/{id}", (int id) =>
{
    var index = hotels.FindIndex(h => h.Id ==id);
    if (index < 0)
    {
        throw new Exception("Not found");
    }
    hotels.RemoveAt(index);
});
app.Run();

public class Hotel
{
    public int Id { get; set; }
    public string Name { get; set; }= String.Empty;
    public double Latitude  { get; set; }
    public double Longitude { get; set; }
}